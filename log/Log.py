import json


class Event:
    def __init__(self, time_stamp, content):
        self.ts = time_stamp
        self.content = content

    def __str__(self):
        return '{{ts: {}, content: {}}}'.format(self.ts, self.content)

    def __repr__(self):
        return '{{ts: {}, content: {}}}'.format(self.ts, self.content)


class Log:
    def __init__(self, obj_type, events):
        self.objType = obj_type
        self.events = events

    def __str__(self):
        return '{{objType: {}, events: {}}}'.format(self.objType, self.events)

    def __repr__(self):
        return '{{objType: {}, events: {}}}'.format(self.objType, self.events)
