def merge_dicts(*dict_args):
    result = {}
    for dic in dict_args:
        result.update(dic)
    return result


def dict_extend(_list, *elements):
    for e in elements:
        _list[e.objType] = e


def list_filter(_list, filter):
    new_line = []
    for line in _list:
        if line.find(filter) != -1:
            new_line.append(line)

    return new_line
