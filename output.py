import utils.str_util as str_util
import utils.time_util as time_util
import os
from log.dot_graph import Graph, Edge


def print_mini(trace):
    for comp, logs in trace.items():
        if comp != 'knative-activator':
            for log in logs.values():
                for e_type, event in log.events.items():
                    if e_type == 'end':
                        time = (event.ts - log.events['start'].ts).total_seconds()
                        print(comp, log.objType, ' latency: ', time)


def print_result(trace):
    for comp, logs in trace.items():
        if comp != 'knative-activator':
            for log in logs.values():
                for e_type, event in log.events.items():
                    if e_type == 'end':
                        time = (event.ts - log.events['start'].ts).total_seconds()
                        print(event.ts, comp, str_util.font_color(e_type, 'r'), log.objType, '(latency is',
                              str_util.font_color(time, 'c'), ')')
                    elif e_type == 'start':
                        print(event.ts, comp, str_util.font_color(e_type, 'g'), log.objType)
                    elif e_type == 'update':
                        print(event.ts, comp, str_util.font_color(e_type, 'y'))
                    else:
                        print(event.ts, comp, e_type, log.objType)


def print_ascii_graph(trace, start_time):
    g = Graph()
    g.add_edge('request', 'scheduler',
               (trace['kube-scheduler']['pod'].events['start'].ts -
                start_time).total_seconds())

    g.add_edge('scheduler', 'kubelet',
               (trace['kube-scheduler']['pod'].events['end'].ts -
                trace['kube-scheduler']['pod'].events['start'].ts).total_seconds())

    g.add_edge('kubelet', 'sandbox',
               (trace['kubelet']['sandbox'].events['start'].ts -
                trace['kube-scheduler']['pod'].events['end'].ts).total_seconds())

    g.add_edge('sandbox', 'user-container',
               (trace['kubelet']['user-container'].events['start'].ts -
                trace['kubelet']['sandbox'].events['start'].ts).total_seconds())

    g.add_edge('proxy-container', 'ready',
               (trace['user-container']['response'].events['start'].ts -
                trace['kubelet']['user-container'].events['end'].ts).total_seconds())

    g.add_edge('user-container', 'proxy-container',
               (trace['kubelet']['queue-proxy'].events['start'].ts -
                trace['kubelet']['user-container'].events['start'].ts).total_seconds(),  '{ start: front,0; }')

    g.add_edge('user-container', 'user-app',
               (trace['user-container']['app'].events['start'].ts -
                trace['kubelet']['user-container'].events['start'].ts).total_seconds(),  '{ start: front,0; }')

    g.add_edge('user-app', 'ready',
               (trace['user-container']['app'].events['end'].ts -
                trace['user-container']['app'].events['start'].ts).total_seconds())

    g.add_edge('ready', 'response',
               (trace['user-container']['response'].events['end'].ts -
                trace['user-container']['response'].events['start'].ts).total_seconds())


    command = 'echo "{}" | graph-easy'.format(g.to_dot())
    return os.system(command)
