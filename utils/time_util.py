import json
import re
import datetime

timeFormat1 = 1
timeFormat2 = 2


def get_time(s, t_format):
    now = datetime.datetime.utcnow()
    if t_format == 1:
        time = datetime.datetime.strptime(re.search(r'\d{2}:\d{2}:\d{2}.\d{3,6}', s).group(), '%H:%M:%S.%f')
        time = time.replace(now.year, now.month, now.day)
        return time
    elif t_format == 2:
        time = datetime.datetime.strptime(re.search(r'\d{2}:\d{2}:\d{2}', s).group(), '%H:%M:%S')
        time = time.replace(now.year, now.month, now.day)
        return time


def get_latency(s):
    latency = re.search(r'\d+\.\d+', s).group()
    return latency[:-1]

def log_timezone_adjust(logs, hours):
    for obj, log in logs.items():
        for key, event in log.events.items():
            event.ts += datetime.timedelta(hours=hours)
    return logs
