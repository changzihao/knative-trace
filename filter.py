import re
import json
import utils.time_util as time_util
import paramiko
import app_log
import datetime
from log.Log import Log, Event
from utils.struct_util import dict_extend
from utils.struct_util import list_filter


def filter_knative_activator(trace):
    lines = trace.split('\n')
    # print(lines)
    filter_trace = []
    k_trace_id = ''
    e2e_latency = ''
    for line in lines:
        if line.find('httpRequest') != -1 and line.find('"status": 200') != -1:
            res = json.loads(line)
            filter_trace.append(line)
            k_trace_id = re.search(r'[a-zA-Z\d]+', res['traceId']).group()
            e2e_latency = time_util.get_latency(res['httpRequest']['latency'])
    return k_trace_id, e2e_latency, filter_trace


def filter_kube_scheduler(trace, _filter=None):
    lines = trace.split('\n')
    if _filter is not None:
        lines = list_filter(lines, _filter)
    logs = {}
    schedule_log = Log('pod', {})
    node = ''
    pod_name = ''
    for line in lines:
        if line.find('About to try and schedule pod') != -1:
            schedule_log.events['start'] = Event(time_util.get_time(line, time_util.timeFormat1), line)
            pod_name = re.search(r'[a-z0-9]([-a-z0-9]*[a-z0-9])?/[a-z0-9]([-a-z0-9]*[a-z0-9])?', line)\
                .group().split('/')
        elif line.find('is bound successfully on') != -1:
            schedule_log.events['end'] = Event(time_util.get_time(line, time_util.timeFormat1), line)
            node = re.search(r'on node [-a-zA-Z0-9]*', line).group().split(' ')[-1]

    dict_extend(logs, schedule_log)
    time_util.log_timezone_adjust(logs, 8)
    return node, pod_name, logs


def filter_kube_controller(trace, _filter=None):
    lines = trace.split('\n')
    if _filter is not None:
        lines = list_filter(lines, _filter)
    logs = {}
    pod_log = Log('pod', {})
    deployment_log = Log('deployment-replica', {})
    for line in lines:
        if line.find('created pod') != -1:
            pod_log.events['start'] = Event(time_util.get_time(line, time_util.timeFormat1), line)
        elif line.find('availableReplicas 0->1') != -1 and line.find('replica_set_utils') != -1:
            pod_log.events['end'] = Event(time_util.get_time(line, time_util.timeFormat1), line)
        elif line.find('Desired pod count change') != -1:
            deployment_log.events['update'] = Event(time_util.get_time(line, time_util.timeFormat1), line)

    dict_extend(logs, pod_log, deployment_log)
    time_util.log_timezone_adjust(logs, 8)
    return logs


def filter_kubelet(node, ts, _filter=None):
    trace = get_kubelet_log(node, ts)
    lines = trace.split('\n')
    print(lines)
    if _filter is not None:
        lines = list_filter(lines, _filter)
    logs = {}
    pod_hash = ''
    pod_log = Log('pod', {})
    sandbox_log = Log('sandbox', {})
    user_log = Log('user-container', {})
    queue_proxy_log = Log('queue-proxy', {})
    for line in lines:
        # print(line)
        if line.find('Receiving a new pod') != -1:
            pod_log.events['start'] = Event(time_util.get_time(line, time_util.timeFormat1), line)
            pod_hash = line[line.find('(') + 1: line.find(')')]
        elif line.find('Creating sandbox for pod') != -1 and line.find(pod_hash) != -1:
            sandbox_log.events['start'] = Event(time_util.get_time(line, time_util.timeFormat1), line)
        elif line.find('Created PodSandbox') != -1 and line.find(pod_hash) != -1:
            sandbox_log.events['end'] = Event(time_util.get_time(line, time_util.timeFormat1), line)
        elif line.find('Creating container') != -1:
            if line.find('user-container') != -1:
                user_log.events['start'] = Event(time_util.get_time(line, time_util.timeFormat1), line)
            elif line.find('queue-proxy') != -1:
                queue_proxy_log.events['start'] = Event(time_util.get_time(line, time_util.timeFormat1), line)
        elif line.find('Started container') != -1 and line.find(pod_hash) != -1:
            if line.find('user-container') != -1:
                user_log.events['end'] = Event(time_util.get_time(line, time_util.timeFormat1), line)
            elif line.find('queue-proxy') != -1:
                queue_proxy_log.events['end'] = Event(time_util.get_time(line, time_util.timeFormat1), line)
        elif line.find('Patch status for pod') != -1 and line.find(pod_hash) != -1 \
                and line.count('ready\\\":true') == 2:
            pod_log.events['end'] = Event(time_util.get_time(line, time_util.timeFormat1), line)

    dict_extend(logs, pod_log, sandbox_log, user_log, queue_proxy_log)
    return logs


def filter_container(trace, k_trace_id, language):
    lines = trace['queue-proxy'].split('\n')
    latency = ''
    for line in lines:
        if line.find('httpRequest') != -1 and line.find('"status": 200') != -1:
            latency = time_util.get_latency(json.loads(line)['httpRequest']['latency'])
    logs = app_log.log_process[language](trace['user-container'], latency)
    time_util.log_timezone_adjust(logs, 8)
    return logs


def get_kubelet_log(node, ts):
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    private = paramiko.RSAKey.from_private_key_file('/home/czh/.ssh/id_rsa')
    client.connect(hostname=node, port=22, username='czh', pkey=private)
    cmd = 'journalctl -u kubelet -S \'{}\''.format(ts)
    print(cmd)
    stdin, stdout, stderr = client.exec_command(cmd)
    # print('out', stdout.read().decode('utf-8'))
    # print('err', stderr.read().decode('utf-8'))
    return stdout.read().decode('utf-8')
