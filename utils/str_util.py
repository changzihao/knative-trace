def font_color(s, color):
    color_s = str(s)
    if color == 'r' or color == 'red':
        color_s = '\033[31m' + color_s + '\033[0m'
    elif color == 'g' or color == 'green':
        color_s = '\033[32m' + color_s + '\033[0m'
    elif color == 'y' or color == 'yellow':
        color_s = '\033[33m' + color_s + '\033[0m'
    elif color == 'b' or color == 'blue':
        color_s = '\033[34m' + color_s + '\033[0m'
    elif color == 'p' or color == 'purple':
        color_s = '\033[35m' + color_s + '\033[0m'
    elif color == 'c' or color == 'cyan':
        color_s = '\033[36m' + color_s + '\033[0m'
    return color_s
