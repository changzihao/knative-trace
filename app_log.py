import utils.time_util as time_util
from log.Log import Log, Event
import datetime
from utils.struct_util import dict_extend

def get_go_log(trace, latency):
    lines = trace.split('\n')
    logs = {}
    init_log = Log('app', {})
    response_log = Log('response', {})
    for line in lines:
        if line.find('Hello world sample started.') != -1:
            init_log.events['start'] = Event(time_util.get_time(line, time_util.timeFormat1), line)
        elif line.find('helloworld: listening on port') != -1:
            init_log.events['end'] = Event(time_util.get_time(line, time_util.timeFormat1), line)
        elif line.find('received a request') != -1:
            start = time_util.get_time(line, time_util.timeFormat1)
            end = start + datetime.timedelta(seconds=float(latency))
            response_log.events['start'] = Event(start, line)
            response_log.events['end'] = Event(end, line)
    dict_extend(logs, init_log, response_log)
    return logs

def get_java_spring_log(trace, latency):
    lines = trace.split('\n')
    logs = {}
    jvm_log = Log('jvm', {})
    app_log = Log('app', {})
    response_log = Log('response', {})

    for line in lines:
        if line.find('JVM running for') != -1:
            end = time_util.get_time(line, time_util.timeFormat1)
            times = line.split(':')[-1].split('(')
            for i in range(len(times)):
                times[i] = time_util.get_latency(times[i])

            app_start = end - datetime.timedelta(seconds=float(times[0]))  # also mean jvm start end
            jvm_start = end - datetime.timedelta(seconds=float(times[1]))

            jvm_log.events['start'] = Event(jvm_start, line)
            jvm_log.events['end'] = Event(app_start, line)

            app_log.events['start'] = Event(jvm_start, line)
            app_log.events['end'] = Event(end, line)

        elif line.find('DispatcherServlet') != -1 and line.find('Completed initialization in') != -1:
            response_log.events['end'] = Event(time_util.get_time(line, time_util.timeFormat1), line)

        elif line.find('Initializing Spring DispatcherServlet') != -1 and line.find('[Tomcat].[localhost].[/]') != -1:
            response_log.events['start'] = Event(time_util.get_time(line, time_util.timeFormat1), line)

    dict_extend(logs, jvm_log, app_log, response_log)
    return logs


def return_all(trace, latency):
    logs = {}
    all_log = Log('logger', {})
    all_log.events['print'] = Event(datetime.datetime.utcnow(), trace)
    dict_extend(logs, all_log)
    return logs


log_process = {
    'go':           get_go_log,
    'python':       return_all,
    'php':          return_all,
    'node.js':      return_all,
    'ruby':         return_all,
    'java-spring':  get_java_spring_log,
    'java-spark':   return_all,
}
