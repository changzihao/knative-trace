import requests
import datetime
import utils.str_util as str_util
import output
import filter
import argparse
import re

from utils.struct_util import merge_dicts


hello_url = 'http://172.18.13.250:31380'
hello_headers = {
    'go':           'helloworld-go.default.example.com',
    'python':       'helloworld-python.default.example.com',
    'php':          'helloworld-php.default.example.com',
    'node.js':      'helloworld-nodejs.default.example.com',
    'ruby':         'helloworld-ruby.default.example.com',
    'java-spring':  'helloworld-java-spring.default.example.com',
    'java-spark':   'helloworld-java-spark.default.example.com',
}

component_info = {
    'kube-scheduler':
        {'url': 'http://localhost:8001/api/v1/namespaces/kube-system/pods/kube-scheduler-ubuntu-master/log'},
    'kube-controller':
        {'url': 'http://localhost:8001/api/v1/namespaces/kube-system/pods/kube-controller-manager-ubuntu-master/log'},
    'knative-activator':
        {'url': 'http://localhost:8001/api/v1/namespaces/knative-serving/pods/{}/log'},
    # 'knative-controller' :
    #     {'url': 'http://localhost:8001/api/v1/namespaces/knative-serving/pods/controller-6f4d6b5f94-hjd8h/log'},
    # 'kube-apiserver':
    #     {'url': 'http://localhost:8001/api/v1/namespaces/kube-system/pods/kube-apiserver-ubuntu-master/log'},
}


container_info = {
    'queue-proxy':
        {'url': 'http://localhost:8001/api/v1/namespaces/{}/pods/{}/log', 'para': {'container': 'queue-proxy'}},
    'user-container':
        {'url': 'http://localhost:8001/api/v1/namespaces/{}/pods/{}/log', 'para': {'container': 'user-container'}},
}


def get_activator_name():
    _url = 'http://localhost:8001/api/v1/namespaces/knative-serving/pods'
    response = requests.get(_url).text
    activator_name = re.search(r'activator-[a-zA-Z\d]+-[a-zA-Z\d]+', response).group()
    component_info['knative-activator']['url'] = component_info['knative-activator']['url'].format(activator_name)
    # print(component_info['knative-activator']['url'])


def set_args():
    parser = argparse.ArgumentParser(description='Trace Cold Start of Knative Hello World Services(python, go, php, '
                                                 'ruby, java-spring, java-spark, node.js...)')

    parser.add_argument('-l', dest='language',
                        action='store', choices=hello_headers.keys(),
                        default='go', help='language of hello world sevice')

    parser.add_argument('--language', dest='language',
                        action='store', choices=hello_headers.keys(),
                        default='go', help='language of hello world sevice')

    parser.add_argument('-o', dest='out_style',
                        action='store', choices={'detail', 'general', 'container', 'mini'},
                        default='general', help='out style')

    parser.add_argument('--out-style', dest='out_style',
                        action='store', choices={'detail', 'general', 'container', 'mini'},
                        default='general', help='out style')

    parser.add_argument('-s', dest='service',
                        action='store', help='service name used to filter log')

    parser.add_argument('--service', dest='service',
                        action='store', help='service name used to filter log')

    return parser


def get_trace(args):
    get_activator_name()
    if (args.out_style != 'mini'):
        print('call head:', hello_headers[args.language])

    now = datetime.datetime.utcnow()
    res = requests.get(hello_url, headers={'Host': hello_headers[args.language]})
    end = datetime.datetime.utcnow()
    print("request time latency is :", (end - now).total_seconds())

    call_time = now.isoformat('T') + 'Z'

    if(args.out_style != 'mini'):
        print('response:', res.text)
        print(call_time)

    mytrace = {}

    paraTime = {'sinceTime': call_time}
    for component, dictionary in component_info.items():
        mytrace[component] = requests.get(dictionary['url'], paraTime).text

    k_traceId, e2e_latency, mytrace['knative-activator'] = filter.filter_knative_activator(mytrace['knative-activator'])
    if float(e2e_latency) > 1.0:
        node, podName, mytrace['kube-scheduler'] = filter.filter_kube_scheduler(mytrace['kube-scheduler'],
                                                                                    args.service)
        mytrace['kube-controller'] = filter.filter_kube_controller(mytrace['kube-controller'], args.service)
        mytrace['kubelet'] = filter.filter_kubelet(node, now + datetime.timedelta(hours=8), args.service)
        # print(trace['kubelet'])

        mytrace['user-container'] = {}
        for name, container in container_info.items():
            url = container['url'].format(podName[0], podName[1])
            paraAll = merge_dicts(paraTime, container['para'])
            mytrace['user-container'][name] = requests.get(url, paraAll).text

        mytrace['user-container'] = filter.filter_container(mytrace['user-container'], k_traceId, args.language)

        if args.out_style != 'mini':
            print('Cold start:', k_traceId + '\'s', 'e2e latency is', str_util.font_color(e2e_latency, 'c'), ',',
                'pod is scheduled on', node)
        else:
            print('Cold start: e2e latency: ', e2e_latency, 'scheduled on', node)

        # pod latency info
        if(args.out_style == 'mini'):
            output.print_mini(mytrace)
        else:
            output.print_result(mytrace)
            output.print_ascii_graph(mytrace, now)

    else:
        print('Warm start:', k_traceId + '\'s', 'e2e latency is', str_util.font_color(e2e_latency, 'b'))

    if args.out_style == 'detail':
        for component, traces in mytrace.items():
            print('------------{}-------------'.format(component))
            print(traces)
    elif args.out_style == 'container':
        print(mytrace['user-container'])


if __name__ == '__main__':
    myparser = set_args()
    args = myparser.parse_args()
    get_trace(args)
