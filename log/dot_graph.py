class Edge:
    def __init__(self, fr, to, value=None, style=None):
        self.fr = fr
        self.to = to
        self.value = value
        self.style = style


    def __repr__(self):
        e = ''
        if self.value == None:
            e =  '[{}] -> [{}] '.format(self.fr, self.to)
        else:
            e = '[{}] -- {} --> [{}]'.format(self.fr, self.value, self.to)
        if self.style != None:
            e = e.replace('>', '> {} '.format(self.style))
        return e


class Graph:
    def __init__(self, nodes=None, edges=None):
        self.nodes = nodes
        self.edges = edges


    def add_node(self, node):
        if self.nodes == None:
            self.nodes = [node]
        else:
            self.nodes.append(node)


    def add_edge(self, fr, to, value=None, style=None):
        edge = Edge(fr, to, value, style)
        if self.edges == None:
            self.edges = [edge]
        else:
            self.edges.append(edge)


    def to_dot(self):
        dot = ''
        if self.nodes != None:
            for n in self.nodes:
                dot += '[{}] '.format(n)
        if self.edges != None:
            for e in self.edges:
                dot += str(e) + ' '
        return dot


